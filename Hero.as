﻿package  {
	
	import flash.display.MovieClip;
	import flash.geom.Point;
	
	
	public class Hero extends NPC {
		
		
		var goal:Point = new Point();
		var tile:Tile;
		var statePlay:StatePlay;
		var rads:Number = 0;
		
		public function Hero(statePlay:StatePlay) {
			arrow.visible = false;
			animSlash.stop();
			animHP.stop();
			this.statePlay = statePlay;
			halfW = 10;
			halfH = 10;
		}
		public function warp(p:Point):void {
			x = p.x;
			y = p.y;
			statePlay.level.discoverAt(x, y);
		}
		public function setGoal(p:Point):void {
			goal = p.clone().add(new Point(.5, .5));
			goal.x *= Tile.size;
			goal.y *= Tile.size;
		}
		override public function update(level:Level):void {
			
			var ax:Number = 0;
			var ay:Number = 0;
			
			if(Keys.W) ay -= ac;
			if(Keys.A) ax -= ac;
			if(Keys.S) ay += ac;
			if(Keys.D) ax += ac;
			
			if(ax != 0 && ay != 0) { // 45 degree angle, use cos/sin(45):
				ax *= .7;
				ay *= .7;
			}
			/*
			if(ax != 0 || ay != 0){
				var a:Number = Math.atan2(ay, ax);
				rotation = a * r2d;
			}
			*/
			var mousex = parent.mouseX - x;
			var mousey = parent.mouseY - y;
			rads = Math.atan2(mousey, mousex);
			//inner.rotation = rads * Global.r2d;
			
			if(rads < 0){
				if(rads > -Global.rad60){
					inner.gotoAndStop(4);
				} else if(rads > -Global.rad120){
					inner.gotoAndStop(5);
				} else {
					inner.gotoAndStop(6);
				}
			} else {
				if(rads < Global.rad60){
					inner.gotoAndStop(3);
				} else if(rads < Global.rad120){
					inner.gotoAndStop(2);
				} else {
					inner.gotoAndStop(1);
				}
			}
			
			animSlash.rotation = rads * Global.r2d;;
			
			// acceleration:
			sx += ax;
			sy += ay;
			
			super.update(level);
			
			tile = statePlay.level.getTileAt(x, y);
			
			arrow.rotation = Math.atan2(goal.y - y, goal.x - x) * Global.r2d;
		}
		override public function heal(num:int):void {
			super.heal(num);
			animHP.play();
		}
		override public function drop():Item {
			var i = super.drop();
			inner.alpha = 1;
			statePlay.gui.updateText("");
			statePlay.gui.box.visible = false;
			if(statePlay.gui.box.item){
				statePlay.gui.box.removeChild(statePlay.gui.box.item);
				statePlay.gui.box.item = null;
			}
			return i;
		}
		override public function equip(item:Item):void {
			super.equip(item);
			if(!this.item) return;
			
			var gui = statePlay.gui;
			var box = gui.box;
			
			gui.updateText(this.item.text);
			if(box.item) box.removeChild(box.item);
			box.item = this.item;
			box.item.x = 0;
			box.item.y = 0;
			box.addChild(box.item);
			box.visible = true;
			if(this.item.type == 10) inner.alpha = 0;
			gui.drawVignette(this.item.type == 6);
		}
		override public function hurt(num:int):void {
			if(item && item.type == 9) num *= .25;
			super.hurt(num);
			new SoundHit().play();
		}
		override public function force(a:Number, f:Number):void {
			if(item && item.type == 9) f *= .5;
			super.force(a, f);
		}
	}
}
