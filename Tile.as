﻿package  {
	import flash.display.MovieClip;
	
	public class Tile extends AABB {

		public static var size:int = 32;
		public var collide:Boolean = true;
		public var type:int = 0;
		public var outline:MovieClip;
		public var hidden:Boolean = true;
		public var item:Item;
		
		public var gridX:int = 0;
		public var gridY:int = 0;
		
		public function Tile(type:int, gridX:Number, gridY:Number) {
			this.gridX = gridX;
			this.gridY = gridY;
			x = gridX * Tile.size;
			y = gridY * Tile.size;
			
			halfH = halfW = size/2;
			offsetX = offsetY = halfW;
			setType(type);
			updateAABB();
			cacheAsBitmap = false;
		}
		public function setType(type:int):void {
			if(type == 3 && Math.random() > .8) type = 4;
			this.type = type;
			gotoAndStop(type);
			collide = false;
			if(type == 1 || type == 3 || type == 4 || type == 7 || type == 8 || type == 15 || type == 16) collide = true;
		}
		override public function collidesWith(o:AABB):Boolean {
			
			if(super.collidesWith(o)){
				if(o is Hero){
					if(type == 3) { // open door:
						setType(2);
						hidden = true;
						Global.levelDoors ++;
						new SoundDoor().play();
					}
					if(type == 4 && Hero(o).item && Hero(o).item.type == 1){ // open locked door:
						setType(2);
						hidden = true;
						Hero(o).drop();
						Global.addPoints(200);
						Global.levelKeys ++;
						Global.levelDoors ++;
						new SoundDoor().play();
					}
					if(type == 5){ // open chest:
						setType(2);
						drop(Item.randItem(true, .05));
						Global.addPoints(100);
						Global.levelChests ++;
					}
					if(type == 14 && Hero(o).item && Hero(o).item.type == 1){ // open locked chest:
						setType(2);
						drop(Item.randItem(true, .10));
						Hero(o).drop();
						Global.addPoints(500);
						Global.levelChests ++;
						Global.levelKeys ++;
					}
					if(type == 6){ // exit level
						Global.levelEnded = true;
					}
				}
				if(collide == false) return false;
				return true;
			}
			return false;
		}
		public function pickup():Item {
			var i = item;
			removeChild(item);
			item = null;
			return i;
		}
		public function drop(item:Item):void {
			if(!item) return;
			this.item = item;
			item.x = Tile.size/2;
			item.y = Tile.size/2;
			addChild(item);
		}
	}
}
