﻿package  {
	
	import flash.display.MovieClip;
	import flash.geom.Point;
	import flash.display3D.IndexBuffer3D;
	
	public class Level extends MovieClip {

		var tiles1:Array = new Array();
		var W:int = 70;
		var H:int = 70;
		
		var origin:Point = new Point();
		var goal:Point = new Point();
		var spawns:Array = new Array();
		var spawnRate:Number = 0;
		public var trapChance:Number = 0;
		public var traps:Array = new Array();

		public function Level() {
		}
		public function update(edgeL:int, edgeT:int):void {
			for(var Y:int = 0; Y < tiles1.length; Y++){
				var rowVisible = false;
				if(edgeT > -Tile.size && edgeT < Global.screenH) rowVisible = true;
				var countX:int = edgeL;
				for(var X:int = 0; X < tiles1[Y].length; X++){
					if(tiles1[Y][X]){
						if(rowVisible && !tiles1[Y][X].hidden){
							tiles1[Y][X].visible = (countX > -Tile.size && countX < Global.screenW);
						} else {
							tiles1[Y][X].visible = false;
						}
					}
					countX += Tile.size;
				}
				edgeT += Tile.size;
			}
		}
		public function generate():void {
			this.removeChildren();
			spawnRate = 0;
			trapChance = Global.level * .02;
			if(trapChance >.3) trapChance = .3;
			
			var t:Array = new Array();
			for(var i:int = 0; i < H; i++){
				var row:Array = new Array();
				for(var j:int = 0; j < W; j++){
					row.push(0);
				}
				t.push(row);
			}
			
			var roomX:int = 30;
			var roomY:int = 30;
			var roomW:int = Global.randInt(4, 8);
			var roomH:int = Global.randInt(4, 8);
			
			origin = new Point(int(roomX + roomW/2) * Tile.size, int(roomY + roomH/2) * Tile.size);
			
			makeRoom(t, [roomX, roomY, roomX + roomW, roomY + roomH], 5);
			
			outline(t);
			build(t);
			chooseGoal();
			getTileAt(origin.x, origin.y).setType(7);
			
			Global.levelTotalDoors = getAllOfType(3).length + getAllOfType(4).length;
			Global.levelTotalChests = getAllOfType(5).length + getAllOfType(14).length;
			
			traps = getAllOfType(15);
		}
		public function makeRoom(t:Array, d:Array, chain:int, DIR:int = 0):void{
			
			var L:int = d[0];
			var T:int = d[1];
			var R:int = d[2];
			var B:int = d[3];
			
			if(--chain < 0) return;
			
			if(DIR != 0){
				if(DIR == 1 && T < 5) return;
				if(DIR == 2 && this.W - R < 5) return;
				if(DIR == 3 && this.H - B < 5) return;
				if(DIR == 4 && L < 5) return;
				
				var MX:int = int((L + R)/2);
				var MY:int = int((T + B)/2);
				var DIS:int = Global.randInt(2, 5);
				var W:int = Global.randInt(4, 10);
				var H:int = Global.randInt(4, 10);
				
				if(DIR == 1) {
					B = T - DIS;
					T = 0;
					for(var i1 = 0; i1 < DIS; i1++) setTile(MX, B+i1, 3, t);
					L = MX - W/2;
					R = 0;
				}
				if(DIR == 2){
					L = R + DIS;
					R = 0;
					for(var i2 = 0; i2 < DIS; i2++) setTile(L-1-i2, MY, 3, t);
					T = MY - H/2;
					B = 0;
				}
				if(DIR == 3){
					T = B + DIS;
					B = 0;
					for(var i3 = 0; i3 < DIS; i3++) setTile(MX, T-1-i3, 3, t);
					R = MX + W/2;
					L = 0;
				}
				if(DIR == 4){
					R = L - DIS;
					L = 0;
					for(var i4 = 0; i4 < DIS; i4++) setTile(R+i4, MY, 3, t);
					B = MY + H/2;
					T = 0;
				}
				
				if(L == 0) L = R - W;
				if(T == 0) T = B - H;
				if(B == 0) B = T + H;
				if(R == 0) R = L + W;
			}
			
			var itemChance:Number = 0;
			var itemGenerated:Boolean = false;
			var chestGenerated:Boolean = false;
			
			var rockChance:Number = Math.random() * .2;
			if(DIR == 0) rockChance = 0;
			for(var Y:int = T; Y < B; Y++){
				for(var X:int = L; X < R; X++){
					
					if(Math.random() < rockChance){
						setTile(X, Y, 8, t);
					} else {
						setTile(X, Y, 2, t);
					}
					if(!chestGenerated && Math.random() < .01){
						setTile(X, Y, (Math.random() > .8) ? 14 : 5, t);
						
					}
					if(!itemGenerated && Y > T && Y < B - 1 && X > L && X < R - 1){
						itemChance += .01;
						if(Math.random() < itemChance){
							itemGenerated = true;
							setTile(X, Y, 99, t);
						}
					}
				}
			}
			/*
			if(goal.x == 0 || Math.random() < .1){
				goal.x = int((L + R)/2);
				goal.y = int((T + B)/2);
				if(goal.x < 1) goal.x = 1;
				if(goal.y < 1) goal.y = 1;
				if(goal.x >= this.W - 1) goal.x = this.W - 2;
				if(goal.y >= this.H - 1) goal.y = this.H - 2;
			}*/
			
			if(chain > 2){
				if(DIR != 3) makeRoom(t, [L, T, R, B], chain, 1);
				if(DIR != 4) makeRoom(t, [L, T, R, B], chain, 2);
				if(DIR != 1) makeRoom(t, [L, T, R, B], chain, 3);
				if(DIR != 2) makeRoom(t, [L, T, R, B], chain, 4);
			} else {
				makeRoom(t, [L, T, R, B], chain, DIR);
			}
		}
		public function chooseGoal():void {
			var a:Number = Math.random() * 2 * Math.PI;
			var m:Number = 1500;
			var success:Boolean = false;
			var tries:int = 0;
			while(!success){
				tries ++;
				a += .1;
				m -= 64;
				var X:int = int((origin.x + m * Math.cos(a))/Tile.size);
				var Y:int = int((origin.y + m * Math.sin(a))/Tile.size);

				var T0:Tile = getTile(X, Y);
				var T1:Tile = getTile(X+1, Y);
				var T2:Tile = getTile(X-1, Y);
				var T3:Tile = getTile(X, Y+1);
				var T4:Tile = getTile(X, Y-1);
				if(T0 && T1 && T2 && T3 && T4){
					if(T0.type == 2) {
						success = true;
						T0.setType(6);
						goal.x = X;
						goal.y = Y;
					}
				}
				/*
				var mark = new Marker();
				mark.x = X * Tile.size + Tile.size/2;
				mark.y = Y * Tile.size + Tile.size/2;
				addChild(mark);
				*/
			}
			//trace(tries + " tries");
		}
		public function dropItems(num:int):void {
			
			
			for(var i:int = 0; i < num; i++){
				var success = false;
				while(!success){
					var X:int = Global.randInt(1, W);
					var Y:int = Global.randInt(1, H);
					var t:Tile = getTile(X, Y);
					if(t && t.type == 2){
						t.drop(Item.randItem());
						success = true;
					}
				}
			}
		}
		public function setTile(X:int, Y:int, val:int, t:Array):void {
			if(X < 0 || Y < 0) return;
			if(Y >= t.length || X >= t[Y].length) return;
			t[Y][X] = val;
		}
		public function getTileFrom(X:int, Y:int, t:Array):int {
			if(X < 0 || Y < 0) return 0;
			if(Y >= t.length || X >= t[Y].length) return 0;
			return t[Y][X];
		}
		public function tunnelFrom(t:Array, p:Point):Point {
			var LEN:int = Global.randInt(6, 12);
			var WID:int = Global.randInt(1, 4);
			var DIR:int = Global.randInt(1, 5);
			
			if(DIR == 1 && p.y < 20) DIR = 3;
			if(DIR == 2 && W - p.x < 20) DIR = 4;
			if(DIR == 3 && H - p.y < 20) DIR = 1;
			if(DIR == 4 && p.x < 20) DIR = 2;
			
			for(var i:int = 0; i < LEN; i++){
				
				if(DIR == 1) p.y --;
				if(DIR == 2) p.x ++;
				if(DIR == 3) p.y ++;
				if(DIR == 4) p.x --;
				
				setTile(p.x, p.y, 1, t);
			}
			return p;
		}
		public function outline(t:Array):void {
			for(var Y:int = 0; Y < t.length; Y++){
				for(var X:int = 0; X < t[Y].length; X++){
					
					if(Y == 0 || X == 0 || Y == t.length - 1 || X == t[Y].length - 1){
						if(t[Y][X] > 1){
							// edges of the world turn to walls:
							t[Y][X] = 1;
						}
					}
					// four adjacent:
					var t1 = getTileFrom(X, Y-1, t);
					var t2 = getTileFrom(X, Y+1, t);
					var t3 = getTileFrom(X-1, Y, t);
					var t4 = getTileFrom(X+1, Y, t);
					// four corners:
					var t5 = getTileFrom(X-1, Y-1, t);
					var t6 = getTileFrom(X-1, Y+1, t);
					var t7 = getTileFrom(X+1, Y+1, t);
					var t8 = getTileFrom(X+1, Y-1, t);
					
					if(t[Y][X] == 0){
						var wall:int = (Math.random() < trapChance) ? 15 : 1;
						// if there is a void next to a tile, turn the void into a wall:
						if(t1 > 1) t[Y][X] = wall;
						if(t2 > 1) t[Y][X] = wall;
						if(t3 > 1) t[Y][X] = wall;
						if(t4 > 1) t[Y][X] = wall;
					}
					if(t[Y][X] == 0){
						// if there is a void diagonal from a non-wall tile, turn the void into a wall:
						if(t5 > 1) t[Y][X] = 1;
						if(t6 > 1) t[Y][X] = 1;
						if(t7 > 1) t[Y][X] = 1;
						if(t8 > 1) t[Y][X] = 1;
					}
					if(t[Y][X] == 8){
						// if a rock is next to a door, remove it:
						if(t1 == 3 || t2 == 3 || t3 == 3 || t4 == 3) t[Y][X] = 2;
						if(t1 == 4 || t2 == 4 || t3 == 4 || t4 == 4) t[Y][X] = 2;
					}
					if(t[Y][X] == 3){
						// if a door is next to another door, remove it:
						if(t1 == 3 || t2 == 3 || t3 == 3 || t4 == 3) t[Y][X] = 2;
						if(t1 == 4 || t2 == 4 || t3 == 4 || t4 == 4) t[Y][X] = 2;
						
						// if a door doesn't block movement, remove it:
						var count = 0;
						if(t1 == 2) count++;
						if(t2 == 2) count++;
						if(t3 == 2) count++;
						if(t4 == 2) count++;
						if(count > 2) t[Y][X] = 2;
					}
				}
			}
		}
		public function build(t:Array):void {
			tiles1 = new Array();
			for(var Y:int = 0; Y < t.length; Y++){
				var row:Array = new Array();
				for(var X:int = 0; X < t[Y].length; X++){
					if(t[Y][X] > 0){
						
						var dropItem:Boolean = false;
						if(t[Y][X] == 2){
							if(Math.random() > .95) t[Y][X] = Global.randInt(9, 14);
						}
						if(t[Y][X] == 99){
							dropItem = true;
							t[Y][X] = 2;
						}
						
						var tile = new Tile(t[Y][X], X, Y);
						row.push(tile);
						addChild(tile);
						
						if(dropItem){
							tile.drop(Item.randItem());
						}
						
					} else {
						row.push(null);
					}
				}
				tiles1.push(row);
			}
		}
		public function discoverAt(X:Number, Y:Number, allowSpawn:Boolean = true):void {
			discover(int(X / Tile.size), int(Y / Tile.size), allowSpawn);
		}
		public function discover(X:int, Y:int, allowSpawn:Boolean = true):void {
			var t = getTile(X, Y);
			if(t && t.hidden){
				t.hidden = false;
				// line-of-sight blocked by walls and doors:
				if(t.type != 1 && t.type != 3 && t.type != 4 && t.type != 15 && t.type != 16){
					discover(X-1, Y);
					discover(X+1, Y);
					discover(X, Y-1);
					discover(X, Y+1);
					discover(X-1, Y-1);
					discover(X+1, Y+1);
					discover(X+1, Y-1);
					discover(X-1, Y+1);
					
					if(allowSpawn && !t.collide && Math.random() < spawnRate){
						spawns.push(gridToWorldCentered(new Point(X, Y)));
					}
				}
			}
		}
		public function gridToWorldCentered(p:Point):Point{
			p.x = p.x * Tile.size + Tile.size/2;
			p.y = p.y * Tile.size + Tile.size/2;
			return p;
		}
		public function getTileAt(X:Number, Y:Number):Tile {
			var row:int = Y / Tile.size;
			var col:int = X / Tile.size;
			return getTile(col, row);
		}
		public function getTile(X:int, Y:int):Tile{
			if(X < 0 || Y < 0) return null;
			if(Y >= tiles1.length || X >= tiles1[Y].length) return null;
			return tiles1[Y][X];
		}
		public function solveCollision(o:AABB):Point {
			var tileTL:Tile = getTileAt(o.edgeL, o.edgeT);
			var tileTR:Tile = getTileAt(o.edgeR, o.edgeT);
			var tileBR:Tile = getTileAt(o.edgeR, o.edgeB);
			var tileBL:Tile = getTileAt(o.edgeL, o.edgeB);
			
			var overlap:Point = new Point();
			
			var colTL:Boolean = tileTL && tileTL.collidesWith(o);
			var colTR:Boolean = tileTR && tileTR.collidesWith(o);
			var colBR:Boolean = tileBR && tileBR.collidesWith(o);
			var colBL:Boolean = tileBL && tileBL.collidesWith(o);
			
			if(tileTL) discoverAt(tileTL.x, tileTL.y, false);
			if(tileTR) discoverAt(tileTR.x, tileTR.y, false);
			if(tileBL) discoverAt(tileBL.x, tileBL.y, false);
			if(tileBR) discoverAt(tileBR.x, tileBR.y, false);
			
			if(!colTL && !colTR && !colBL && !colBR) return overlap;
			
			if(colTL && colTR) overlap = overlap.add(o.overlapWith(tileTL.combineWith(tileTR)));
			if(colTL && colBL) overlap = overlap.add(o.overlapWith(tileTL.combineWith(tileBL)));
			if(colBL && colBR) overlap = overlap.add(o.overlapWith(tileBL.combineWith(tileBR)));
			if(colTR && colBR) overlap = overlap.add(o.overlapWith(tileTR.combineWith(tileBR)));
			
			if(colTL && !colTR && !colBL) overlap = overlap.add(o.overlapWith(tileTL));
			if(colTR && !colTL && !colBR) overlap = overlap.add(o.overlapWith(tileTR));
			if(colBL && !colBR && !colTL) overlap = overlap.add(o.overlapWith(tileBL));
			if(colBR && !colBL && !colTR) overlap = overlap.add(o.overlapWith(tileBR));
			
			return overlap;
		}
		public function getAllOfType(num:int):Array{
			var out:Array = new Array();
			for each(var row:Array in tiles1){
				for each(var tile:Tile in row){
					if(tile && tile.type == num) out.push(tile);
				}
			}
			return out;
		}
	}
}
