﻿package  {
	import flash.display.MovieClip;
	public class Item extends MovieClip{

		public var type:int = 0;
		public var text:String = "";
		public var desc:String = "";
		public var cost:int = 0;
		public var dmgBase:int = 0;
		public var dmgRange:int = 0;
		public var dmgCritMult:Number = 0;
		public var dmgCritChance:Number = 0;
		public var lastHitWasCrit:Boolean = false;

		public static function randItem(includeUncommon:Boolean = false, rareBoost:Number = 0):Item {
			
			var max:int = 8;
			if(includeUncommon) max += Global.randInt(1, 7);
			return new Item(Global.randInt(1, max), rareBoost);
		}

		public function Item(type:int, rareBoost:Number = 0) {
			setType(type, rareBoost);
			cacheAsBitmap = true;
		}
		public function setType(type:int, rareBoost:Number = 0):void {
			this.type = type;
			gotoAndStop(type);
			switch(type){
				case 1: text = "KEY"; break;
				case 2: text = "POTION"; break;
				case 3: text = "SWORD"; makeWeapon(true, rareBoost); break;
				case 4: text = "COMPASS"; break;
				case 5: text = "BOMB"; break;
				case 6: text = "TORCH"; break;
				case 7: text = "WAND"; makeWeapon(false, rareBoost); break;
				case 8: text = "BURST WAND"; makeWeapon(false, rareBoost); break;
				case 9: text = "ARMOR"; break;
				case 10: text = "INVISIBILITY CLOAK"; break;
				case 11: text = "CONJURING SPELLBOOK"; break;
				case 12: text = "KNOWLEDGE SPELLBOOK"; break;
				case 13: text = "TELEKINETIC SPELLBOOK"; break;
			}
			buildDesc();
		}
		public function makeWeapon(melee:Boolean, rareBoost:Number = 0):void {
			
			var altType:Boolean = Math.random() < .5;
			
			if(melee){
				if(altType) {
					text = "AXE";
					inner.gotoAndStop(3);
				} else {
					inner.gotoAndStop(1);
				}
			} else {
				
				
			}
			
			var bonus:int = Global.randInt(0, Global.level);
			var rareChance:Number = (.01 + rareBoost) * Global.level;
			if(rareChance > .5) rareChance = .5;
			var rare:Boolean = Math.random() < rareChance;
			
			dmgBase = melee ? Global.randInt(2, 11) : Global.randInt(2, 10);
			dmgRange = melee ? Global.randInt(2, 11) : Global.randInt(1, 5);
			dmgCritMult = melee ? Global.randInt(1, 4) + Global.randInt(1, 4) : Global.randInt(2, 4);
			dmgCritChance = melee ? Global.randInt(1, 5) + Global.randInt(0, 2) + Global.randInt(0, 2) : Global.randInt(1, 5);
			dmgCritChance *= 5 / 100;
			
			var costMult:Number = melee ? 1 : 2;
			
			if(rare){
				if(melee) {
					inner.gotoAndStop(inner.currentFrame + 1);
				}
				dmgBase ++;
				dmgRange ++;
				switch(Global.randInt(1, 5)){
					case 1:
						text = "LEGENDARY " + text;
						dmgBase *= 2;
						break;
					case 2:
						text = "BRUTAL " + text;
						dmgCritMult *= 2;
						break;
					case 3:
						text = "MYTHIC " + text;
						dmgCritChance *= 2;
						break;
					case 4:
						switch(Global.randInt(1, 5)){
							case 1: text = "ANTIQUE " + text; costMult *= 2; break;
							case 2: text = "HISTORIC " + text; costMult *= 3; break;
							case 3: text = "FORGOTTEN " + text; costMult *= 4; break;
							case 4: text = "RELIC " + text; costMult *= 5; break;
						}
						break;
				}
			}
			
			cost = (dmgBase + dmgRange) * 10 + (dmgCritChance / .05) * dmgCritMult * 10;
			cost *= costMult;
			if(!rare){
				if(melee){
					if(dmgBase == 2) text = "PATHETIC " + text;
					if(dmgBase == 3) text = "BROKEN " + text;
					if(dmgBase == 4) text = "RUSTY " + text;
					if(dmgBase == 8) text = "STRONG " + text;
					if(dmgBase == 9) text = "SHARP " + text;
					if(dmgBase == 10) text = "HIGH-QUALITY " + text;
					if(dmgRange == 2) text = "TINY " + text;
					if(dmgRange == 3) text = "LIGHT " + text;
					if(dmgRange == 9) text = "HEAVY " + text;
					if(dmgRange == 10) text = "MASSIVE " + text;
					if(dmgCritChance >= .25) text = "LUCKY " + text;
					
					if(altType){ // axe
						dmgBase -= 2;
						dmgRange += 2;
						dmgCritMult ++;
					}
				} else {
					if(dmgCritChance >= .20) text = "LUCKY " + text;
				}
			}
			if(bonus > 0){
				dmgBase += bonus;
				text += " +" + bonus;
			}
			
			text += " (" + dmgBase + " - " + (dmgBase + dmgRange) + ")";
		}
		public function buildDesc():void {
			
		}
		public function calcDmg():int {
			lastHitWasCrit = (Math.random() < dmgCritChance);
			var mult:Number = lastHitWasCrit ? dmgCritMult : 1;
			var dmg:Number = dmgBase + Math.random() * dmgRange;
			dmg *= mult;
			
			return int(dmg);
		}
	}
}
