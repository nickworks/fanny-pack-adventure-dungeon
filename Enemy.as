﻿package  {
	import flash.geom.Point;
	public class Enemy extends NPC{

		public var target:NPC;
		public var score:int = 100;
		public var speed:Number = 10;
		public var sight:Number = 200;
		private var type:Number = 1;
		public var attackType:Number = 1;
		private var delay:int = 0;
		private var delayBase:int = 0;
		var afraidOfLight:Boolean = false;
		
		public function Enemy(p:Point) {
			x = p.x;
			y = p.y;
			setType(Global.randInt(1, 4));
		}
		public function setType(num:int):void {
			type = num;
			gotoAndStop(type);
			
			delay = Math.random() * 40 + 20;
			switch(type){
				case 1:
					score = 100;
					afraidOfLight = true;
					delayBase = 40;
					attackType = 1;
					blood = 0x00FF00;
					hp = 20;
					halfW = 12;
					halfH = 8;
					break;
				case 2:
					score = 200;
					afraidOfLight = true;
					delayBase = 20;
					attackType = 1;
					blood = 0x00ADFF;
					hp = 20;
					halfW = 12;
					halfH = 8;
					break;
				case 3:
					score = 200;
					afraidOfLight = false;
					delayBase = 40;
					attackType = 1;
					blood = 0xFF0000;
					hp = 20;
					halfW = 12;
					halfH = 8;
					break;
			}
		}
		override public function update(level:Level):void {
			var a:Number = 0;
			var f:Number = 0;
			
			var ghost:Boolean = (target && target.item && target.item.type == 10);
			
			if(--delay < 0){
				delay = Math.random() * delayBase + delayBase;
				if(target && !ghost){
					if(attackType == 1){
						var v:Point = Global.mag2AndAngle(new Point(x, y), new Point(target.x, target.y));
						if(v.x < sight * sight){
							a = v.y;
							f = speed;
						}
					}
				}
			}
			if(afraidOfLight && target.item && target.item.type == 6) f *= -1;
			
			force(a, f);
			super.update(level);
		}
		public function stun(num:Number):void {
			delay = Math.random() * num + num;
		}
		public function calcDmg():int {
			return 10;
		}
		public function drops():int {
			var out:int = 0;
			if(Math.random() < 1){
				out = Global.randInt(0, 7);
				if(out == 3 || out == 4 || out == 6) out = 0;
			}
			
			return out;
		}
	}
}
