﻿package  {
	import flash.geom.Point;
	
	public class Global {

		public static var score:int = 0;
		public static var level:int = 1;
		public static var screenW:int = 550;
		public static var screenH:int = 400;
		public static var r2d:Number = 180/Math.PI;
		public static var d2r:Number = Math.PI/180;
		public static var scoreMult:Number = 1;
		public static var radL:Number = Math.PI;
		public static var radU:Number = -Math.PI/2;
		public static var radR:Number = 0;
		public static var radD:Number = Math.PI/2;
		
		public static var rad45:Number = Math.PI/4;
		public static var rad60:Number = Math.PI/3;
		public static var rad120:Number = Math.PI * 2 / 3;
		
		public static function addPoints(num:int):void {
			num *= scoreMult;
			score += num;
			levelPoints += num;
		}
		
		public static var levelItem:Item = null;
		public static var levelHP:Number = 100;
		public static var levelEnded:Boolean = false;
		public static var levelPoints:Number = 0;
		public static var levelChests:Number = 0;
		public static var levelKills:Number = 0;
		public static var levelKeys:Number = 0;
		public static var levelDmgDealt:Number = 0;
		public static var levelDmgRecvd:Number = 0;
		public static var levelTime:Number = 0;
		public static var levelDoors:Number = 0;
		public static var levelTotalChests:Number = 0;
		public static var levelTotalDoors:Number = 0;
		
		public static function levelReset():void {
			levelEnded = false;
			levelPoints = 0;
			levelChests = 0;
			levelKills = 0;
			levelKeys = 0;
			levelDmgDealt = 0;
			levelDmgRecvd = 0;
			levelTime = 0;
			levelDoors = 0;
			levelTotalChests = 0;
			levelTotalDoors = 0;
			scoreMult = 1;
		}
		public static function gameReset():void {
			level = 1;
			levelItem = null;
			levelHP = 100;
			score = 0;
		}
		
		public static function randInt(min:int, max:int):int{
			return int(Math.random() * (max - min) + min);
		}
		public static function mag2AndAngle(p1:Point, p2:Point):Point {
			var dx = p2.x - p1.x;
			var dy = p2.y - p1.y;
			var m = dx * dx + dy * dy;
			var a = Math.atan2(dy, dx);
			return new Point(m, a);
		}
	}
}
